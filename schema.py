from enum import Enum
import json
from typing import Optional, Literal

from pydantic import BaseModel, Field
from pydantic.fields import ModelField


class WithMarkdownDescription(object):
    @classmethod
    def __modify_schema__(cls, field_schema, field: Optional[ModelField]):
        if 'description' in field_schema:
            field_schema['markdownDescription'] = field_schema['description']


class ModelWithMarkdownDescription(BaseModel):
    class Config:
        @staticmethod
        def schema_extra(schema, model) -> None:
            if 'description' in schema:
                schema['markdownDescription'] = schema['description']


class KeyCodeStr(WithMarkdownDescription, Enum):
    """
    References:
    - [karabiner-complex-rules-generator/src/constants.ts](https://github.com/genesy/karabiner-complex-rules-generator/blob/source/src/constants.ts)
    - [Karabiner-Elements/simple_modifications.json](https://github.com/pqrs-org/Karabiner-Elements/blob/main/src/apps/PreferencesWindow/Resources/simple_modifications.json)
    """

    num_1 = '1'
    num_2 = '2'
    num_3 = '3'
    num_4 = '4'
    num_5 = '5'
    num_6 = '6'
    num_7 = '7'
    num_8 = '8'
    num_9 = '9'
    num_0 = '0'
    a = 'a'
    b = 'b'
    c = 'c'
    d = 'd'
    e = 'e'
    f = 'f'
    g = 'g'
    h = 'h'
    i = 'i'
    j = 'j'
    k = 'k'
    l = 'l'
    m = 'm'
    n = 'n'
    o = 'o'
    p = 'p'
    q = 'q'
    r = 'r'
    s = 's'
    t = 't'
    u = 'u'
    v = 'v'
    w = 'w'
    x = 'x'
    y = 'y'
    z = 'z'
    f1 = 'f1'
    f2 = 'f2'
    f3 = 'f3'
    f4 = 'f4'
    f5 = 'f5'
    f6 = 'f6'
    f7 = 'f7'
    f8 = 'f8'
    f9 = 'f9'
    f10 = 'f10'
    f11 = 'f11'
    f12 = 'f12'
    f13 = 'f13'
    f14 = 'f14'
    f15 = 'f15'
    f16 = 'f16'
    f17 = 'f17'
    f18 = 'f18'
    f19 = 'f19'
    escape = 'escape'
    grave_accent_and_tilde = 'grave_accent_and_tilde'
    tab = 'tab'
    caps_lock = 'caps_lock'
    spacebar = 'spacebar'
    up_arrow = 'up_arrow'
    left_arrow = 'left_arrow'
    down_arrow = 'down_arrow'
    right_arrow = 'right_arrow'
    return_or_enter = 'return_or_enter'
    backslash = 'backslash'
    open_bracket = 'open_bracket'
    close_bracket = 'close_bracket'
    semicolon = 'semicolon'
    quote = 'quote'
    comma = 'comma'
    period = 'period'
    slash = 'slash'
    hyphen = 'hyphen'
    equal_sign = 'equal_sign'
    delete_or_backspace = 'delete_or_backspace'
    display_brightness_decrement = 'display_brightness_decrement'
    display_brightness_increment = 'display_brightness_increment'
    mission_control = 'mission_control'
    launchpad = 'launchpad'
    illumination_decrement = 'illumination_decrement'
    illumination_increment = 'illumination_increment'
    rewind = 'rewind'
    play_or_pause = 'play_or_pause'
    fastforward = 'fastforward'
    mute = 'mute'
    volume_decrement = 'volume_decrement'
    volume_increment = 'volume_increment'


class Modifier(WithMarkdownDescription, Enum):
    caps_lock = 'caps_lock'
    left_command = 'left_command'
    left_control = 'left_control'
    left_option = 'left_option'
    left_shift = 'left_shift'
    right_command = 'right_command'
    right_control = 'right_control'
    right_option = 'right_option'
    right_shift = 'right_shift'
    fn = 'fn'
    command = 'command'
    control = 'control'
    option = 'option'
    shift = 'shift'
    left_alt = 'left_alt'
    left_gui = 'left_gui'
    right_alt = 'right_alt'
    right_gui = 'right_gui'
    any = 'any'


class Modifiers(ModelWithMarkdownDescription):
    """
    Reference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/from/modifiers/
    """
    mandatory: list[Modifier] | None = Field(None, markdownDescription='Modifiers which must be pressed.')
    optional: list[str] | None = Field(None, markdownDescription='Modifiers which can be pressed.')


class FromEvent(ModelWithMarkdownDescription):
    # key_code, consumer_key_code, pointing_button and any are exclusive.
    # You have to specify one of them. 

    key_code: Optional[KeyCodeStr | int] = Field(None, markdownDescription='Key code which you want to change.')
    # consumer_key_code: ... = Field(None, markdownDescription='Consumer key code (media key code) which you want to change.')
    # pointing_button: ... = Field(None, markdownDescription='Pointing button name which you want to change.')
    # any: ... = Field(None, markdownDescription='"any": "key_code", "any": "consumer_key_code" or "any": "pointing_button".')
    modifiers: Modifiers | None = Field(None, markdownDescription='Specify mandatory and optional modifiers.')
    # simultaneous: ... = Field(None, markdownDescription='Specify multiple events which are pressed simultaneously.')
    # simultaneous_options: ... = Field(None, markdownDescription='Options for simultaneous.')


class ToEvent(ModelWithMarkdownDescription):
    key_code: Optional[KeyCodeStr | int] = Field(None, markdownDescription='Key code which you want to post')
    consumer_key_code: Optional[object] = Field(None, markdownDescription='Consumer key code (media key code) which you want to post')
    pointing_button: Optional[object] = Field(None, markdownDescription='Pointing button name which you want to post')
    shell_command: Optional[str] = Field(None, markdownDescription='Shell command which you want to execute\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/shell-command/')

    # https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/select-input-source/
    select_input_source: Optional[object] = Field(None, markdownDescription='Input source which you want to switch')

    mouse_key: Optional[object] = Field(None, markdownDescription='A mouse key definition\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/mouse-key/')
    sticky_modifier: Optional[dict[Modifier, Literal["on", "off", "toggle"]]] = Field(None, markdownDescription='A sticky modifier key definition\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/sticky-modifier/')
    software_function: Optional[object] = Field(None, markdownDescription='A software function definition\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/software_function/')
    modifiers: Optional[list[Modifier]] = Field(None, markdownDescription='Modifiers which are post with the event\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/modifiers/')
    lazy: Optional[bool] = Field(None, markdownDescription='Lazy modifier flag\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/lazy/')
    repeat: Optional[bool] = Field(None, markdownDescription='Key repeat flag\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/repeat/')
    halt: Optional[bool] = Field(None, markdownDescription='A flag for to_after_key_up\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/halt/')
    hold_down_milliseconds: Optional[int] = Field(None, markdownDescription='Interval of key_down and key_up when these events are sent at the same time\n\nReference: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/to/hold-down-milliseconds/')


class Manipulator(ModelWithMarkdownDescription):
    """
    Reference: [complex_modifications manipulator definition][1]

    [1]: https://karabiner-elements.pqrs.org/docs/json/complex-modifications-manipulator-definition/
    """

    type: Literal["basic"] = Field(..., markdownDescription='Always `"basic"`.')
    from_: FromEvent = Field(..., alias='from', markdownDescription='The name of key code, consumer key code or pointing button which you want to change.')
    to: Optional[list[ToEvent]] = Field(None, markdownDescription='Events which are sent when you press `from` key.')
    to_if_alone: Optional[list[ToEvent]] = Field(None, markdownDescription='Events which are sent when you press `from` key alone.')
    to_if_held_down: Optional[list[ToEvent]] = Field(None, markdownDescription='Events which are sent when you hold down `from` key.')
    to_after_key_up: Optional[list[ToEvent]] = Field(None, markdownDescription='Events which are sent after you release `from` key.')
    to_delayed_action: Optional[list[ToEvent]] = Field(None, markdownDescription='Events which are sent after 500 milliseconds at you press `from` key.')
    # conditions: Optional[...] = Field(None, markdownDescription='Manipulator is applied only if condition is matched (e.g., the frontmost application).')
    # parameters: Optional[...] = Field(None, markdownDescription='Override parameters such as `to_if_alone_timeout_milliseconds`.')
    description: Optional[str] = Field(None, markdownDescription='A human-readable comment.')


class Rule(ModelWithMarkdownDescription):
    description: Optional[str]
    manipulators: list[Manipulator] = ...


class Root(ModelWithMarkdownDescription):
    title: str = ...
    rules: list[Rule] = ...

    class Config:
        @staticmethod
        def schema_extra(schema, model) -> None:
            ModelWithMarkdownDescription.Config.schema_extra(schema, model)
            # schema['$id'] = 'https://codeberg.org/notpushkin/karabiner-jsonschema/raw/branch/master/schema.json'
            schema['title'] = 'Karabiner Elements Complex Modification file'


if __name__ == '__main__':
    print(json.dumps(Root.schema(), indent=2))
